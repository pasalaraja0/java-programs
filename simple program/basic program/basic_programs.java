1)write a program to add 2 numbers
import java.util.Scanner;
public class AddTwoNumbers2 {

    public static void main(String[] args) {
        
        int num1, num2, sum;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter First Number: ");
        num1 = sc.nextInt();
        
        System.out.println("Enter Second Number: ");
        num2 = sc.nextInt();
        
        sc.close();
        sum = num1 + num2;
        System.out.println("Sum of these numbers: "+sum);
    }
}

2)Write a Java program to divide two numbers and print on the screen.
import java.util.Scanner;

public class Main {
  public static void main(String[] args) 
  {
    Scanner input = new Scanner (System.in);
    System.out.print("Input the first number: ");
    int a = input.nextInt();
    System.out.print("Input the second number: ");
    int b = input.nextInt();
    int d = (a/b);
    System.out.println();
    System.out.println("The division of a and b is:" +d);
  }
}
3)Write a Java program to print the result of the following operations(+,-,*,/,%).

public class BasicArithmeticDemo
{
 
    public static void main(String[] args)
    {
        int number1 = 10;
        int number2 = 5;
 
        //calculating number1 + number2;
        int sum = number1 + number2;
 
        //calculating number1 - number2;
        int difference = number1 - number2;
 
        //calculating number1 * number2;
        int product = number1 * number2;
 
        //calculating number1 / number2;
        int quot = number1 / number2;
 
        //calculating number1 % number2;
        int rem = number1 % number2;
 
        //Displaying the values
        System.out.println("number1 : "+number1);
        System.out.println("number2 : "+number2);
        System.out.println("sum : "+sum);
        System.out.println("difference : "+difference);
        System.out.println("product : "+product);
        System.out.println("quot : "+quot);
        System.out.println("rem : "+rem);
    }
}
4)Write a Java program that takes a number as input and prints its multiplication table.import java.util.Scanner;
 
public class Exercise7 {
 
 public static void main(String[] args) {
  Scanner in = new Scanner(System.in);
   
  System.out.print("Input a number: ");
  int num1 = in.nextInt();
   
  for (int i=0; i< 10; i++){
   System.out.println(num1 + " x " + (i+1) + " = " + 
     (num1 * (i+1)));
  }
 }
}
5)public class Exercise8 {
 
    public static void main(String[] args) {

        System.out.println("   J    a   v     v  a ");
        System.out.println("   J   a a   v   v  a a");
        System.out.println("J  J  aaaaa   V V  aaaaa");
        System.out.println(" JJ  a     a   V  a     a");
    }
}
6)Write a Java program to compute the specified expressions and print the output.
Specified Expression : 
(25.5 * 3.5 - 3.5 * 3.5) / (40.5 - 4.5)

public class Exercise9 {
 
    public static void main(String[] arg) {

        System.out.println((25.5 * 3.5 - 3.5 * 3.5) / (40.5 - 4.5));
    }
}
7)Write a Java program to print the area and perimeter of a circle.
public class Exercise11 {
 
   private static final double radius = 7.5;

    public static void main(String[] args) {

        double perimeter = 2 * Math.PI * radius;
        double area = Math.PI * radius * radius;

        System.out.println("Perimeter is = " + perimeter);
        System.out.println("Area is = " + area);
    }
}

8)Write a Java program that takes five numbers as input to calculate and print the average of the numbers.
import java.util.Scanner;
 
public class Exercise12 {
 
 public static void main(String[] args) {
  Scanner in = new Scanner(System.in);
   
  System.out.print("Input first number: ");
  int num1 = in.nextInt();
   
  System.out.print("Input second number: ");
  int num2 = in.nextInt();
   
  System.out.print("Input third number: ");
  int num3 = in.nextInt();
   
  System.out.print("Input fourth number: ");
  int num4 = in.nextInt();
  
  System.out.print("Enter fifth number: ");
  int num5 = in.nextInt();
   
   
  System.out.println("Average of five numbers is: " + 
   (num1 + num2 + num3 + num4 + num5) / 5);
 }
}
9) Write a Java program to print the area and perimeter of a rectangle.
public class Exercise13 {
 
   public static void main(String[] strings) {

        final double width = 5.6;
        final double height = 8.5;

        double perimeter = 2*(height + width);
		
        double area = width * height;			
		
		System.out.printf("Perimeter is 2*(%.1f + %.1f) = %.2f \n", height, width, perimeter);

        System.out.printf("Area is %.1f * %.1f = %.2f \n", width, height, area);
    }
}
10)Write a Java program to print an American flag on the screen.
public class Exercise14 {
 
   public static void main(String[] args)
    {
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
    }
}
11)Write a Java program to swap two variables.
public class Exercise15 {
 
 public static void main(String[] args) {
     
   int a, b, temp;
   a = 15;
   b = 27;
   System.out.println("Before swapping : a, b = "+a+", "+ + b);
   temp = a;
   a = b;
   b = temp;   
  System.out.println("After swapping : a, b = "+a+", "+ + b);
 }
 }
12)program to print a face
public class Exercise16 {
    public static void main(String[] args)
    {
        System.out.println(" +\"\"\"\"\"+ ");
        System.out.println("[| o o |]");
        System.out.println(" |  ^  | ");
        System.out.println(" | '-' | ");
        System.out.println(" +-----+ ");
    }
	}
13)Write a Java program to check whether Java is installed on your computer.
public class Exercise31 {
public static void main(String[] args) {
    System.out.println("\nJava Version: "+System.getProperty("java.version"));
    System.out.println("Java Runtime Version: "+System.getProperty("java.runtime.version"));
    System.out.println("Java Home: "+System.getProperty("java.home"));
    System.out.println("Java Vendor: "+System.getProperty("java.vendor"));
    System.out.println("Java Vendor URL: "+System.getProperty("java.vendor.url"));
    System.out.println("Java Class Path: "+System.getProperty("java.class.path")+"\n");
 }
}

14)Write a Java program to compute the area of a hexagon.

import java.util.Scanner;
public class Exercise34 {
      public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input the length of a side of the hexagon: ");
        double s = input.nextDouble();
        System.out.print("The area of the hexagon is: " + hexagonArea(s)+"\n");
    }
    public static double hexagonArea(double s) {
        return (6*(s*s))/(4*Math.tan(Math.PI/6));
    }
}
15)Write a Java program to print the ascii value of a given character.
public class Exercise41 {

	public static void main(String[] String) {
		int chr = 'Z';
		System.out.println("The ASCII value of Z is :"+chr);
	}
}

16)Write a Java program to display the system time.
public class Exercise46 {
  
  public static void main(String[] args){
         System.out.format("\nCurrent Date time: %tc%n\n", System.currentTimeMillis());
    }
}
17)read number of inches and convert into meters
import java.util.Scanner;
public class Exercise2 {

    public static void main(String[] Strings) {

        Scanner input = new Scanner(System.in);

        System.out.print("Input a value for inch: ");
        double inch = input.nextDouble();
        double meters = inch * 0.0254;
        System.out.println(inch + " inch is " + meters + " meters");

    }
}
18)Write a Java method to find the smallest number among three numbers
import java.util.Scanner;
public class Exercise1 {

 public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Input the first number: ");
        double x = in.nextDouble();
        System.out.print("Input the Second number: ");
        double y = in.nextDouble();
        System.out.print("Input the third number: ");
        double z = in.nextDouble();
        System.out.print("The smallest value is " + smallest(x, y, z)+"\n" );
    }

   public static double smallest(double x, double y, double z)
    {
        return Math.min(Math.min(x, y), z);
    }
}
19) average of three numbers
 import java.util.Scanner;
public class Exercise2 {

 public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Input the first number: ");
        double x = in.nextDouble();
        System.out.print("Input the second number: ");
        double y = in.nextDouble();
        System.out.print("Input the third number: ");
        double z = in.nextDouble();
        System.out.print("The average value is " + average(x, y, z)+"\n" );
    }

  public static double average(double x, double y, double z)
    {
        return (x + y + z) / 3;
    }
}
20)Write a Java program to convert a string to an integer in Java
import java.util.*;
public class Exercise51 {

 public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Input a number(string): ");
        String str1 = in.nextLine();
		int result = Integer.parseInt(str1);
        System.out.printf("The integer value is: %d",result);
		System.out.printf("\n");
    }
}