package jdbcconnect;

import java.util.Scanner;

public class Abstraction {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a,b;
		Scanner s=new Scanner(System.in);
		System.out.println("Enter first number");
		a=s.nextInt();
		System.out.println("Enter second number");
		b=s.nextInt();
		N obj=new N();
		obj.numbers(a,b);
		obj.sum();
	}
}
abstract class M
{
	abstract void numbers(int i,int j);
	abstract void sum();
}
class N extends M
{
	public int a,b,c;
	public void numbers(int i,int j)
	{
		this.a=i;
		this.b=j;
		
	}
	public void sum()
	{
		c=a+b;
		System.out.println("sum = "+c);
	}
}